json.array!(@quotes) do |quote|
  json.extract! quote, :id, :duration, :interest, :quote_status, :vehicle_id, :customer_id, :employee_id
  json.url quote_url(quote, format: :json)
end
