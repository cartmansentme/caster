json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :make, :model, :body, :year, :msrp, :vin, :vehicle_status
  json.url vehicle_url(vehicle, format: :json)
end
