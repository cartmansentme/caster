json.array!(@employees) do |employee|
  json.extract! employee, :id, :name, :phone, :title_id, :manager_id
  json.url employee_url(employee, format: :json)
end
