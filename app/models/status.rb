class Status < ActiveRecord::Base
  has_many :customers

  validates :status, format: { with: /\A[a-zA-Z]+\z/,
                             message: " Only letters are allowed" }
end
