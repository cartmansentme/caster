class Customer < ActiveRecord::Base
  has_many :quotes
  belongs_to :status

validates_inclusion_of :rating, {:in =>300..850, message: "enter a valid credit rating"}
  validates_numericality_of :phone#, numericality: { only_integer: true }
  validates :name, format: { with: /\A[a-zA-Z]+\z/,
                                    message: "Only letters are allowed " }
  validates_length_of :phone, {:in =>10..15, message: "Phone numbers can have between 10-15 digits" }
  validates :phone, uniqueness: true


end
