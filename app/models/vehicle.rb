class Vehicle < ActiveRecord::Base
  has_many :quotes


  validates :year, numericality: { only_integer: true }
  validates :make, format: { with: /\A[a-zA-Z]+\z/,
                             message: " Only letters are allowed" }

  validates :body, format: { with: /\A[a-zA-Z]+\z/,
                             message: " Only letters are allowed" }
  validates :msrp, numericality: { only_decimal: true }
end
