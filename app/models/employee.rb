class Employee < ActiveRecord::Base
  belongs_to :title
  has_many   :quotes
  has_many   :employees, class_name: 'Employee', foreign_key: 'manager_id'
  belongs_to :manager, class_name: 'Employee'

  validates :phone, numericality: { only_integer: true }
  validates :name, format: { with: /\A[a-zA-Z]+\z/,
                             message: "Only letters are allowed" }
  validates_length_of :phone, {:in =>10..15, message: "Phone numbers can have between 10-15 digits" }
  validates :phone, uniqueness: true



  def correct
    self.manager.nil? ? 'No Manager' : self.manager.name
  end
end
