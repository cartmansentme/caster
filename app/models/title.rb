class Title < ActiveRecord::Base
  has_many :employees
  validates :title, format: { with: /\A[a-zA-Z]+\z/,
                             message: "Only letters are allowed" }
end
