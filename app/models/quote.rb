class Quote < ActiveRecord::Base
  belongs_to :employee
  belongs_to :customer
  belongs_to :vehicle

  validates :duration, numericality: { only_integer: true }
  validates :interest, numericality: { only_decimal: true }
  validates_inclusion_of :duration, {:in =>3..5, message: "Enter 3 or 4 or 5 years for the loan duration" }
end
