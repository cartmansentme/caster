class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :make
      t.string :model
      t.string :body
      t.integer :year
      t.decimal :msrp
      t.string :vin
      t.boolean :vehicle_status

      t.timestamps null: false
    end
  end
end
