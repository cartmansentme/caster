class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :phone, :limit=>10
      t.integer :rating
      t.integer :status_id

      t.timestamps null: false
    end
  end
end
