class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :duration
      t.decimal :interest
      t.boolean :quote_status
      t.integer :vehicle_id
      t.integer :customer_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
