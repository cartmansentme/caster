# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Customer.delete_all
Employee.delete_all
Quote.delete_all
Status.delete_all
Title.delete_all
Vehicle.delete_all


Status.create(status:'Active')
Status.create(status:'Potential')

Title.create(title:'Sales')
Title.create(title:'FinanceManager')
Title.create(title:'DealershipOwner')
Title.create(title:'InventoryManager')

Customer.create(name:'JamesTiberiusKirk', phone:1003340247, rating:800, status_id: 1)
Customer.create(name:'Spock', phone:1104425226, rating:850, status_id: 1)
Customer.create(name:'LeonardMcCoy', phone:1004440321, rating:650, status_id: 1)
Customer.create(name:'MontgomeryScott', phone:1002221337, rating:600, status_id: 2)
Customer.create(name:'NyotaUhura', phone:1105550527, rating:741, status_id: 1)
Customer.create(name:'HikaruSulu', phone:1233340666, rating:830, status_id: 1)
Customer.create(name:'PavelovCheckov', phone:3333540267, rating:500, status_id: 2)



Employee.create(name:'JeanLucPicard', phone:3333540267, title_id: 3)
Employee.create(name:'WilliamRiker', phone:3323540232, title_id: 2, manager_id: 1)
Employee.create(name:'DianaTroy', phone:3323450232, title_id: 1, manager_id: 1)
Employee.create(name:'BeverlyCrusher', phone:2223540266, title_id: 2, manager_id: 1)
Employee.create(name:'Data', phone:1001010011, title_id: 1, manager_id: 1)
Employee.create(name:'GeordiLaForge', phone:1234567890, title_id: 4, manager_id: 1)
Employee.create(name:'Worf', phone:5553331234, title_id: 1, manager_id: 1)

Vehicle.create(make:'Mercedes', model:'C350', body:'Coupe', year:2013, msrp:'54000.00', vin:'MB2013C350C')
Vehicle.create(make:'Mercedes', model:'C350', body:'Sedan', year:2013, msrp:'50000.00', vin:'MB2013C350S')
Vehicle.create(make:'Mercedes', model:'C63', body:'Coupe', year:2013, msrp:'64000.00', vin:'MB2013C350A')
Vehicle.create(make:'Mercedes', model:'C300', body:'Sedan', year:2015, msrp:'45000.00', vin:'MB2015C300S')
Vehicle.create(make:'Mercedes', model:'C400', body:'Sedan', year:2015, msrp:'50000.00', vin:'MB2015C400S')
Vehicle.create(make:'Mercedes', model:'CLA45', body:'Coupe', year:2015, msrp:'54000.00', vin:'MB2015C45CA')
Vehicle.create(make:'BMW', model:'M4', body:'Coupe', year:2015, msrp:'74000.00', vin:'MB2013C350')
Vehicle.create(make:'Porsche', model:'918', body:'Coupe', year:2015, msrp:'800000.00', vin:'PE2015918C')
Vehicle.create(make:'Porsche', model:'Cayman', body:'Coupe', year:2015, msrp:'55000.00', vin:'PE2015CAYC')

Quote.create(duration:3, interest:'3.4', vehicle_id: 8, customer_id: 1, employee_id: 3)
Quote.create(duration:3, interest:'3.4', vehicle_id: 9, customer_id: 2, employee_id: 5)
Quote.create(duration:3, interest:'3.4', vehicle_id: 1, customer_id: 3, employee_id: 6)
Quote.create(duration:4, interest:'3.4', vehicle_id: 2, customer_id: 4, employee_id: 3)
Quote.create(duration:4, interest:'3.4', vehicle_id: 3, customer_id: 4, employee_id: 3)
Quote.create(duration:4, interest:'3.4', vehicle_id: 4, customer_id: 5, employee_id: 5)
Quote.create(duration:5, interest:'3.4', vehicle_id: 5, customer_id: 3, employee_id: 5)
Quote.create(duration:4, interest:'3.4', vehicle_id: 5, customer_id: 6, employee_id: 6)
Quote.create(duration:3, interest:'3.4', vehicle_id: 8, customer_id: 6, employee_id: 6)
Quote.create(duration:5, interest:'3.4', vehicle_id: 6, customer_id: 7, employee_id: 3)
Quote.create(duration:3, interest:'3.4', vehicle_id: 7, customer_id: 1, employee_id: 2)
Quote.create(duration:4, interest:'3.4', vehicle_id: 8, customer_id: 5, employee_id: 4)
